import childs.Circle;
import childs.Item;
import childs.Triangle;
import parents.Product;
import parents.Shape;

public class MainApp {
    public static void main(String[] args) {
//        Product product1 = new Product; //error, karena jika abstract tidack bisa membuat obj langsung
        Product product2 = new Item("Lenovo Z410", 8500000); //buat object dengan instansiasi Item child class

        //object circle
        Shape circle1 = new Circle("Biru", 20);
        Shape triangle1 = new Triangle("Red", 10, 15);

        //print
        System.out.println("HASIL RUMUS LINGKARAN BERWARNA " + circle1.getColor() + " :" + circle1.getArea());
        System.out.println("HASIL RUMUS SEGITIGA BERWARNA " + triangle1.getColor() + " :" + triangle1.getArea());
    }
}
