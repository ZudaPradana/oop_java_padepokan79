package parents;

//yg dimaksud abstract artinya class belum kongkrit
//tidak bisa dibuat obj secara langsung, jadi disempurnakan di child class
//jika ada method abstract, class harus dijadikan abstract
public abstract class Product {
    private String name;

    //construc
    public Product(String name) {
        this.name = name;
    }

    //getter & setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //abstract method wajib tidak ada isi
    //akan di override di child class
    public abstract void printInformation();
}
