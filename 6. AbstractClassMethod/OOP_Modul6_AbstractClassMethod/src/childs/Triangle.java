package childs;
import parents.Shape;
public class Triangle extends Shape{
    private double base;
    private double height;

    //construct
    public Triangle(String color, double base, double height) {
        super(color);
        this.base = base;
        this.height = height;
    }

    //getter & setter
    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    //override method
    @Override
    public double getArea(){
        double area = 0.5 * base * height;
        return area;
    }
}
