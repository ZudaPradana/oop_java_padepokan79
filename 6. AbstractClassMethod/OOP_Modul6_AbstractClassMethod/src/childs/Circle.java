package childs;
import parents.Shape;
public class Circle extends Shape{
    private double radius;

    //construct
    public Circle(String color, double radius) {
        super(color);
        this.radius = radius;
    }

    //getter & setter
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea(){
        double area = Math.PI * radius * radius;
        return area;
    }
}
