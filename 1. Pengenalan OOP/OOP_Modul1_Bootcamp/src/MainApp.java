public class MainApp {
    public static void main(String[] args) {
        //create object person 1
        Person person1 = new Person();
        person1.name = "Eko";
        person1.address = "Tegal";

        //print
        System.out.println(person1.name);
        System.out.println(person1.address);
        System.out.println(person1.country);

        //call method
        person1.sayHello("Padepokan 79");
        System.out.println(person1.sayAddress());

        //create object person 2
        Person person2 = new Person();
        person2.name = "Joko";
        person2.address = "Surabaya";

        //call method
        person2.sayHello("Padepokan 79");

        //call method return
        System.out.println(person2.sayAddress());

        //create object person 3 using constructor
        Person person3 = new Person("Zuda", "Lamongan");

        //call method
        person3.sayHello("Padepokan 79");
        System.out.println(person3.sayAddress());

        //create object person 4 using constructor 1 var
        Person person4 = new Person("Zedd");
        person4.address = "Bandung";
        person4.sayHello("Padepokan 79");
        System.out.println(person4.sayAddress());
    }
}
