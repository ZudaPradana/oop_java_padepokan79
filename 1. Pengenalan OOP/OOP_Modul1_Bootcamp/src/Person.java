public class Person {
    String name;
    String address;
    final String country = "Indonesia";

    //constructor (inisiasi param ketika buat obj)
    //default
    Person(){

    }

    //with param
//    Person(String paramName){
//        name = paramName;
//    }

    //menghindari variabel shadowing (field class dan param sama)
    Person(String name){
        this.name = name;//this mengacu pada field pada class
    }

//    Person(String paramName, String paramAddress){
//        this(paramName);//agar tidak double dengan yg atas
//        address = paramAddress;
//    }

    Person(String name, String address){
        this.name = name;
        this.address = address;
    }

    //method
    void sayHello(String paramName){
        System.out.println("Hello " + paramName + ", My name is " + name + ".");
    }

    //method return
    String sayAddress(){
        return "I, come from " + address + ".";
    }
}
