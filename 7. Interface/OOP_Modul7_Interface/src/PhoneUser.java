//import interface
import Interface.Phone;
public class PhoneUser {
    //variable interface
    private Phone phone;

    //construct
    public PhoneUser(Phone phone){
        this.phone = phone;
    }

    //getter
    public Phone getPhone(){
        return this.phone;
    }
    //setter
    public void setPhone(){
        this.phone = phone;
    }

    public void turnOnThePhone(){
        this.phone.powerOn();
    }

    public void turnOffThePhone(){
        this.phone.powerOff();
    }

    public void makePhoneLouder(){
        this.phone.volumeUp();
    }

    public void makePhoneSilence(){
        this.phone.volumeDown();
    }


}
