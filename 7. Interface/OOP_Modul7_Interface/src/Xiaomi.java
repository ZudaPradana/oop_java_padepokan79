import Interface.Phone;

public class Xiaomi implements Phone {
    private int volume;
    private boolean isPowerOn;

    //getter
    public int getVolume(){
        return this.volume;
    }

    public boolean isPowerOn(){
        return this.isPowerOn;
    }

    //setter
    public void setVolume(int volume){
        this.volume = volume;
    }

    public void setPowerOn(boolean isPowerOn){
        this.isPowerOn = isPowerOn;
    }

    public Xiaomi(){
        //set volume default
        this.volume = 50;
    }

    //override method interface Phone
    @Override
    public void powerOn(){
        isPowerOn = true;
        System.out.println("Handphone menyala ...");
        System.out.println("Selamat datang di Xiaomi");
        System.out.println("Android Version 10");
    }

    @Override
    public void powerOff(){
        isPowerOn = false;
        System.out.println("Mematikan Handphone");
    }

    @Override
    public void volumeUp(){
        if (isPowerOn){
            if(this.volume == MIN_VOLUME){
                System.out.println("Volume = 0%");
            } else {
                this.volume += 10;
                System.out.println("Volume sekarang " + this.volume + "%");
            }
        } else {
            System.out.println("Handphone mati, silahkan nyalakan dulu!!!");
        }
    }

    @Override
    public void volumeDown(){
        if (isPowerOn){
            if(this.volume == MIN_VOLUME){
                System.out.println("Volume = " + this.volume + "%");
            } else {
                this.volume -= 10;
                System.out.println("Volume sekarang " + this.volume + "%");
            }
        } else {
            System.out.println("Handphone mati, silahkan nyalakan dulu!!!");
        }
    }
}
