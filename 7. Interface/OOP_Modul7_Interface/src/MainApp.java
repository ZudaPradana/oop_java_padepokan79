import Interface.Phone;

import java.util.Scanner;

public class MainApp {
    public static void main(String[] args) {
        //jalankan interface
        //buat obj interface phone dari class xiaomi
        //seperti abstract class, interface tidak bisa buat obj langsung, melainkan perlu menggunakan class yg implement (jika di absctract = child)
        Phone redmiNote10 = new Xiaomi();
        //buat obj phoneuser
        PhoneUser findha = new PhoneUser(redmiNote10);
        //turn the phone
        findha.turnOnThePhone();
        //membuat tampilan
        Scanner input = new Scanner(System.in);
        String action;
        boolean isLooping = true;

        do {
            System.out.println("===Aplikasi Interface===");
            System.out.println("[1] Nyalakan HP");
            System.out.println("[2] Matikan HP");
            System.out.println("[3] Perbesar Volume");
            System.out.println("[4] Perkecil Volume");
            System.out.println("[0] Keluar");
            System.out.println("=========================");

            System.out.println("Pilih Aksi: ");
            action = input.next();

            if (action.equalsIgnoreCase("1")){
                findha.turnOnThePhone();
            } else if (action.equalsIgnoreCase("2")) {
                findha.turnOffThePhone();
            } else if (action.equalsIgnoreCase("3")) {
                findha.makePhoneLouder();
            } else if (action.equalsIgnoreCase("4")) {
                findha.makePhoneSilence();
            } else if (action.equalsIgnoreCase("0")){
                isLooping = false;
            } else {
                System.out.println("Error, input again please");
            }
        } while (isLooping);
    }
}
