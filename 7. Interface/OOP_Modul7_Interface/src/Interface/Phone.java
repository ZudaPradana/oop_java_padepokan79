package Interface;
//kegunaan interface sebagai penghubung antara sesuatu yang ‘abstrak’ dengan sesuatu yang nyata.
//method wajib kosong, dan atribut wajib final
public interface Phone {
    //tanpa inisiasi final, interface sudah otomatis final
    //bedakan constant field dengan nama variable capital
    int MAX_VOLUME = 100;
    int MIN_VOLUME = 0;

    //method
    void powerOn();
    void powerOff();
    void volumeUp();
    void volumeDown();
}
