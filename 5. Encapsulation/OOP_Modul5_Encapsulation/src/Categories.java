public class Categories {
    //encapsulation akan menggunakan modifier untuk menjaga atribut obj, semua menggunakan modifier private
    private int id;
    private String name;
    private boolean expensive;

    public Categories(int id, String name, boolean expensive) {
        this.id = id;
        this.name = name;
        this.expensive = expensive;
    }

    public Categories(){
        super();
    }


    //getter dan setter menggunakan public agar bisa diakses yg class lain
    //getter : method untuk mengambil data field
    //jika bolean getter menggunakan isNamavariable
    public boolean isExpensive() {
        return this.expensive;
    }

    //selain tipe boolean menggunakan getNamavariable
    public String getName() {
        return this.name;
    }

    public int getId()  {
        return this.id;
    }

    //setter : method untuk merubah data field
    //semua pemanggilan menggunakan setNamavariable
    public void setExpensive(boolean expensive){
        this.expensive = expensive;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setId(int id){
        this.id = id;
    }
}
