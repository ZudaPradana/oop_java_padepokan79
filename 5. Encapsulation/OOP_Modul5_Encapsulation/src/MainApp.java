//tambahkan import package
import childs.Doctor;
import childs.Programmer;
import childs.Teacher;
import parents.Person;

public class MainApp {
    public static void main(String[] args) {
        //object person
        // menyimpan object programmer disimpan variabel person1 dengan type Person Class
        //hall ini bisa dilakukan karena programmer merupakan child dari Person (parent class)
        Person person1 = new Programmer("Rizky", "Bandung", ".Net Core");

        //object teacher
        Person person2 = new Teacher("Joko", "Tegal", "Matematika");

        //object doctor
        Person person3 = new Doctor("Eko", "Surabaya", "Pedistrician");

        //object Person (kondisi encapsulation)
        Person person4 = new Person();
        person4.setName("Zuda");
        person4.setAddress("Lamongan");

        //memanggil field encapsulation
        System.out.println(person4.getName());
        System.out.println(person4.getAddress());




        //method
        //method person1 akan tetap mengikuti method override di object programmer
        person1.greeting();
        //namun ketika person1 ingin menampilkan variable technology akan gagal, karena type Person tidak memiliki field tersebut
        //System.out.println(person1.technology); // akan error
        //solusinya dengan recasting
        System.out.println(((Programmer) person1).getTechnology());
        System.out.println("=====");


        //check instansiasi
        checkSayHello(person1);
        checkSayHello(person2);
        checkSayHello(person3);

    }

    //type check & casts
    //agar lebih aman biasanya dilakukan check type data menggunakan instanceof
    //hasilnya berupa boolean
    static void checkSayHello(Person person){
        String message;
        if (person instanceof Programmer){
            Programmer programmer = (Programmer) person;
            message = "Hello, " + programmer.getName()+ ". Saya seorang Programmer " + programmer.getTechnology() + ".";
        } else if (person instanceof Teacher){
            Teacher teacher = (Teacher) person;
            message = "Hello, " + teacher.getName() + ". Saya seorang Guru " + teacher.getSubject();
        } else if (person instanceof Doctor) {
            Doctor doctor = (Doctor) person;
            message = "Hello, " + doctor.getName()+ ". Saya seorang Doktor " + doctor.getSpecialist() + ".";
        } else {
            message = "Hello, " + person.getName() + ".";
        }
        System.out.println(message);
    }

}
