package childs;
//tambahkan package parent class
import parents.Person;
//extend parents.Person class as a parent
public class Teacher extends Person {
    //tambahkan modifier public agar child di package lain bisa access
    private String subject;

    //getter
    public String getSubject() {
        return subject;
    }


    //setter
    public void setSubject(String subject) {
        this.subject = subject;
    }

    //constructor
    public Teacher(String name, String address, String subject) {
        super(name, address); //pemanggilan super wajib di block pertama
        this.subject = subject;
    }

    void teaching(){
        System.out.println("I can teach " + subject + ", so anyone how wants to learn can talk to me");
    }

    //overriding method parent
    //ketika overriding, method parent tidak bisa dipanggil dalam bentuk asli
    //cara mengatasinya dengan menggunakan super.namaMethod
    @Override
    //tambahkan modifier public agar child di package lain bisa access
    public void greeting(){
//        System.out.println("Hello my name is " + this.name);
//        System.out.println("I come from " + this.address);
        super.greeting(); //pemanggilan super wajib di block pertama
        System.out.println("My job is a " + this.subject + " teacher");
    }
}
