package parents;

public class Person {
    //field
    //encapsulation menjaga object, dengan memberikan modifier private
    private String name;
    private String address;

    //getter(mendapatkan) dan setter(merubah), menggunakan public agar bisa di akses class lain
    //getter, jika boolean menggunakan isNamaVariabel

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    //setter
    public void setName(String name){
        this.name = name;
    }

    public void setAddress(String address){
        this.address = address;
    }

    //constructor
    public Person(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Person() {

    }

    //method
    public void greeting(){
        System.out.println("Hello my name is " + getName());
        System.out.println("I come from " + getAddress());
    }


}
