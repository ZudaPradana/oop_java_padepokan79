package parents;

public class Person {
    //field
    //tambahkan modifier public agar child di package lain bisa access
    public String name;
    public String address;

    //constructor
    public Person(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Person() {

    }

    //method
    public void greeting(){
        System.out.println("Hello my name is " + this.name);
        System.out.println("I come from " + this.address);
    }


}
