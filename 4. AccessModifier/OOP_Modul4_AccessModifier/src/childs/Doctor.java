package childs;
//tambahkan package parent class
import parents.Person;

//extend Persom
public class Doctor extends Person {
    //tambahkan modifier public agar child di package lain bisa access
    public String specialist;
    //constructor
    public Doctor(String name, String address, String specialist) {
        super(name, address);
        this.specialist = specialist;
    }

    public Doctor(String specialist) {
        this.specialist = specialist;
    }

    //method
    void surgery(){
        System.out.println("I can surgery operation Patients ");
    }

    //overriding method parent
    //ketika overriding, method parent tidak bisa dipanggil dalam bentuk asli
    //cara mengatasinya dengan menggunakan super.namaMethod
    @Override
    //tambahkan modifier public agar child di package lain bisa access
    public void greeting(){
        System.out.println("Hello my name is " + this.name);
        System.out.println("I come from " + this.address);
        System.out.println("My occupation is " + this.specialist + " doctor");
    }
}
