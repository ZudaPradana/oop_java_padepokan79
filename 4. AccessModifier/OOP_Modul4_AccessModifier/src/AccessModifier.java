public class AccessModifier {
    //public = dapat akses semua class
    //public String ....

    //no-modifier = dapat akses ke sub-class, non-subclass selama pada package yg sama
    //String ...

    //private = hanya bisa akses ke class yg sama (sub-class saudaranya)
    //private String ...

    //protected = bisa akses semua kecuali non-sub-class di package berbeda
    //protected String ....


}
