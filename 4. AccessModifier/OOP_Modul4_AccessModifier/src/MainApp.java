//tambahkan import package
import childs.Doctor;
import childs.Programmer;
import childs.Teacher;
import parents.Person;

public class MainApp {
    public static void main(String[] args) {
        //object person
        // menyimpan object programmer disimpan variabel person1 dengan type Person Class
        //hall ini bisa dilakukan karena programmer merupakan child dari Person (parent class)
        Person person1 = new Programmer("Rizky", "Bandung", ".Net Core");

        //object teacher
        Person person2 = new Teacher("Joko", "Tegal", "Matematika");

        //object doctor
        Person person3 = new Doctor("Eko", "Surabaya", "Pedistrician");


        //method
        //method person1 akan tetap mengikuti method override di object programmer
        person1.greeting();
        //namun ketika person1 ingin menampilkan variable technology akan gagal, karena type Person tidak memiliki field tersebut
        //System.out.println(person1.technology); // akan error
        //solusinya dengan recasting
        System.out.println(((Programmer) person1).technology);
        System.out.println("=====");


        //check instansiasi
        checkSayHello(person1);
        checkSayHello(person2);
        checkSayHello(person3);

    }

    //type check & casts
    //agar lebih aman biasanya dilakukan check type data menggunakan instanceof
    //hasilnya berupa boolean
    static void checkSayHello(Person person){
        String message;
        if (person instanceof Programmer){
            Programmer programmer = (Programmer) person;
            message = "Hello, " + programmer.name + ". Saya seorang Programmer " + programmer.technology + ".";
        } else if (person instanceof Teacher){
            Teacher teacher = (Teacher) person;
            message = "Hello, " + teacher.name + ". Saya seorang Guru " + teacher.subject;
        } else if (person instanceof Doctor) {
            Doctor doctor = (Doctor) person;
            message = "Hello, " + doctor.name + ". Saya seorang Doktor " + doctor.specialist + ".";
        } else {
            message = "Hello, " + person.name + ".";
        }
        System.out.println(message);
    }

}
