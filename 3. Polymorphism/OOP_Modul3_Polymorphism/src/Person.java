public class Person {
    //field
    String name;
    String address;

    //constructor
    public Person(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Person() {

    }

    //method
    void greeting(){
        System.out.println("Hello my name is " + this.name);
        System.out.println("I come from " + this.address);
    }


}
