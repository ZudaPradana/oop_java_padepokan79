public class MainApp {
    public static void main(String[] args) {
        //object person
        Person person1 = new Person();
        person1.name = "Hendras";
        person1.address = "Garut";

        //object teacher
        Person teacher1 = new Teacher("Budi", "Bandung", "Matematika");

        //object doctor
        Person doctor1 = new Doctor("Elis", "Jakarta", "Dentis");

        //object programmer
        Person programmer1 = new Programmer("Rizki", "Surabaya", "JavaScript");


        //method
        person1.greeting();
        System.out.println("=====");
        teacher1.greeting();
        System.out.println("=====");
        doctor1.greeting();
        System.out.println("=====");
        programmer1.greeting();
    }
}
