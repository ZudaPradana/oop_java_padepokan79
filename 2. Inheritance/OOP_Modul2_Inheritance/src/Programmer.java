public class Programmer extends Person{
    String technology;
    //constructor
    public Programmer(String name, String address, String technology) {
        super(name, address);
        this.technology = technology;
    }

    public Programmer(String technology) {
        this.technology = technology;
    }

    //method
    void hacking(){
        System.out.println("I can hacking a website");
    }

    void coding(){
        System.out.println("I can create an application using technology " + technology + ".");
    }

    //overriding method parent
    //ketika overriding, method parent tidak bisa dipanggil dalam bentuk asli
    //cara mengatasinya dengan menggunakan super.namaMethod
    @Override
    void greeting(){
        System.out.println("Hello my name is " + this.name);
        System.out.println("I come from " + this.address);
        System.out.println("My job is a " + this.technology + " programmer");
    }
}
